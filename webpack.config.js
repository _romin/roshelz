const path = require('path')

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {

    /**
     * Production is minified.
     */
    mode: 'development',

     /**
     * Show minimal output in terminal.
     */
    stats: 'minimal',

    /**
     * Where is the main js file.
     */
    entry: {
        main: path.resolve(__dirname, './src/js/main.js'),
    },

    /**
     * Export js to the output.
     */
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].bundle.js',
    },

    /**
     * Loaders.
     */
    module: {
        rules: [
            {
                test: /\.s?css$/,
                exclude: /node_modules/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "postcss-loader",
                    "sass-loader",
                    "import-glob-loader",
                ],
            },
        ],
    },

    /**
     * Plugins.
     */
    plugins: [
        new MiniCssExtractPlugin(),
    ],
}
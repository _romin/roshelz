import '../sass/main.scss'

import WOW from 'wow.js';

/**
  * Wow.js (Animations)
  */
const wow = new WOW(
    {
        boxClass: 'animate',
        animateClass: 'animated',
        offset: 100,
        mobile: true,
        live: true,
        scrollContainer: '#wrapper',
        resetAnimation: false,
    },
);
wow.init();

var x = document.getElementById("myAudio");


x.play();
